import babel from "@rollup/plugin-babel";
import { eslint } from "rollup-plugin-eslint"
import resolve from "@rollup/plugin-node-resolve"
import commonjs from "@rollup/plugin-commonjs"


//debugger
import { debug } from "debug";
const log = debug("app:log");

debug.enable("*");
log("Logging is enabled!");

export default {
	input: "src/main.js",
	output: {
		file: "src/bundle.js",
		format: "iife",
		sourcemap: "inline"
	},
	plugins: [
		resolve(),
		commonjs(),
		eslint({
			exclude: "styles/**"
		}),
		babel({
			exclude: "node_modules/**",
			babelHelpers: "bundled"
		}),
	]
};