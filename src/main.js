import { select, csv, create } from "d3";

csv("data/data.csv").then(data => {
  data.forEach(d => {
    d.population = +d.population * 1000;
  });
  console.log(data);
})


let barChart = function() {


// make a bar chart
let barData = [4, 8, 15, 16, 23, 42]

const div = create("div")
  .style("font", "10px sans-serif")
  .style("text-align", "right")
  .style("color", "white");

div.selectAll("div")
  .data(barData)
  .join("div")
  .style("background", "steelblue")
  .style("padding", "3px")
  .style("margin", "1px")
  .style("width", d => `${d * 10}px`)
  .text(d => d);

return div.node();
}
barChart();

//todo chord-diagram
//todo sankey-diagram
